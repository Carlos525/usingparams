package mx.tec.lab;

import static org.junit.jupiter.api.Assertions.*;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import mx.tec.lab.entity.Dragon;
import mx.tec.lab.repository.DragonRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DragonRepositoryTest {
	@Resource
	private DragonRepository dragonRepository;
	
	@Test
	public void givenDragon_whenSave_thenRetrieveSame() {
		Dragon dragon = new Dragon(1, "Meraxes");
		dragonRepository.save(dragon);
		
		Dragon retrievedDragon = dragonRepository.findById(1L).orElse(null);
		assertEquals("Meraxes", retrievedDragon.getName());
	}
	
	@Test
	public void givenDragon_whenModified_thenRetrieveMod() {
		Dragon dragon = new Dragon(1,"Meraxes");
		dragonRepository.save(dragon);
		
		Dragon modifDragon = dragonRepository.findById(1L).orElse(null);
		modifDragon.setName("Chimuelo");
		dragonRepository.save(modifDragon);
		
		Dragon retrievedDragon = dragonRepository.findById(1L).orElse(null);
		assertEquals("Chimuelo", retrievedDragon.getName());
	}
	
	@Test
	public void givenDragon_whenRemoved_thenRetrieveDel(){
		Dragon dragon = new Dragon(1,"Meraxes");
		dragonRepository.save(dragon);
		dragonRepository.deleteById(dragon.getId());
		Dragon retrievedDragon = dragonRepository.findById(1L).orElse(null);
		assertNull(retrievedDragon);
	}
}
